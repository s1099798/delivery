FROM node:10-alpine
WORKDIR /app

COPY ./Delivery/ClientApp ./
RUN npm install

ENTRYPOINT npm run build -- --prod

# docker build --rm -f "Build_Frontend.Dockerfile" -t delivery:build-frontend .
# docker run --rm -it delivery:build-frontend