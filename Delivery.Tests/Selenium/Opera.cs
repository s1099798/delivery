using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Opera;

namespace Delivery.Tests.Selenium
{
    internal class Opera : Browser
    {
        public override string Name => "opera";

        protected override IWebDriver CreateWebDriver()
        {
            var driverDirectory = AppDomain.CurrentDomain.BaseDirectory;
            
            var webdriver = new OperaDriver(driverDirectory);
            PrepareWebDriver(webdriver);

            return webdriver;
        }
    }
}