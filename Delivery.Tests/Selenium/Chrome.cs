using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Delivery.Tests.Selenium
{
    internal class Chrome : Browser
    {
        public override string Name => "chrome";

        protected override IWebDriver CreateWebDriver()
        {
            var options = new ChromeOptions();
            options.AddArgument("headless");
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-dev-shm-usage");

            var driverDirectory = AppDomain.CurrentDomain.BaseDirectory;
            
            var webdriver = new ChromeDriver(driverDirectory, options);
            PrepareWebDriver(webdriver);

            return webdriver;
        }
    }
}