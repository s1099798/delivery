FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
RUN apt-get install -y chromium=73.0.3683.75-1~deb9u1
RUN apt-get install -y firefox-esr

COPY ./Delivery ./Delivery/
COPY ./Delivery.Tests ./Delivery.Tests/
COPY ./Delivery.Tests/Drivers ./Delivery.Tests/bin/Debug/netcoreapp2.2
RUN chmod 777 ./Delivery.Tests/bin/Debug/netcoreapp2.2/chromedriver
COPY ./Delivery.sln .

RUN ["mkdir", "Delivery/ClientApp/node_modules"]

ENTRYPOINT ["dotnet", "test", "--filter", "Category=Functional"]
# ENTRYPOINT ["tail", "-f", "/dev/null"]

# docker build --rm -f "Functional_Test.Dockerfile" -t delivery:functional-test .
# docker run --rm -it delivery:functional-test