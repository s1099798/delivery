FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /app
EXPOSE 80

ENV ASPNETCORE_ENVIRONMENT Production

RUN apt-get update
RUN apt-get install -y zip

COPY ./app.zip ./
RUN unzip app.zip
RUN mv ./output/* ./

ENTRYPOINT ["dotnet", "Delivery.dll"]

# docker build --rm -f "Production.Dockerfile" -t delivery:latest .
# docker run --rm -it -p 80:80 delivery:latest