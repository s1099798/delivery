FROM registry.gitlab.com/s1099798/delivery:base
WORKDIR /app

ENV ASPNETCORE_ENVIRONMENT Production

COPY ./Delivery/*.csproj ./
RUN dotnet restore

COPY ./Delivery ./
RUN dotnet publish -c Release -o output

RUN mkdir /artifacts

# ENTRYPOINT ["tail", "-f", "/dev/null"]
ENTRYPOINT zip -r /artifacts/app.zip ./output/