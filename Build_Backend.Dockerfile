FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

ENV ASPNETCORE_ENVIRONMENT Production

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY ./Delivery/*.csproj ./
RUN dotnet restore

COPY ./Delivery ./
ENTRYPOINT dotnet build -c Release

# docker build --rm -f "Build_Backend.Dockerfile" -t delivery:build-backend .
# docker run --rm -it delivery:build-backend