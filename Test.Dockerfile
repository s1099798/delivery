FROM registry.gitlab.com/s1099798/delivery:base
WORKDIR /app

COPY ./Delivery ./Delivery/
COPY ./Delivery.Tests ./Delivery.Tests/
COPY ./Delivery.Tests/Drivers ./Delivery.Tests/bin/Debug/netcoreapp2.2
RUN chmod 777 ./Delivery.Tests/bin/Debug/netcoreapp2.2/chromedriver
COPY ./Delivery.sln .

RUN ["mkdir", "Delivery/ClientApp/node_modules"]

ENTRYPOINT ["dotnet", "test"]

# docker build --rm -f "Test.Dockerfile" -t delivery:test .
# docker run --rm -it delivery:test