FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY ./Delivery ./Delivery/
COPY ./Delivery.Tests ./Delivery.Tests/
COPY ./Delivery.sln .

ENTRYPOINT ["dotnet", "test", "--filter", "Category=Unit"]

# docker build --rm -f "Unit_Test.Dockerfile" -t delivery:unit-test .
# docker run --rm -it delivery:unit-test