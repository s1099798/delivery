# Sonarqube op server
Sonarqube installeren en rapporten maken op de server

## Sonarqube omgeving starten

| omschrijving                  | actie                                                                           |
| ----------------------------- | ------------------------------------------------------------------------------- |
| verbinding starten met server | putty openen voor server                                                        |
| inloggen server               | ww invoeren                                                                     |
| als root inloggen             | ```su``` -> root ww invoeren                                                    |
| sonarqube opstarten           | ```docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube:latest``` |

## Sonarqube report maken/versturen (gitlab job)
> zorg dat .net sdk gebruikt wordt (microsoft/dotnet:2.2-sdk)

| omschrijving                                                 | actie                                                                                                                                                                                                                                                                                                                                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| installeer sonarqube tool                                    | ```dotnet tool install --global dotnet-sonarscanner```                                                                                                                                                                                                                                                                                                              |
| zoek de sonarscanner executable op, en begin met het rapport | ```~/.dotnet/tools/dotnet-sonarscanner begin /k:"delivery" /d:sonar.login="5c2fc38ed4a2432695aab9b4e9ee50f6eafba787" /d:sonar.host.url=(IP_ADDRESS):9000 /d:sonar.cs.vstest.reportsPaths="TestResults.trx" /d:sonar.coverage.exclusions="**Test*.cs,**Controller*.cs,**.ts,**.js,Startup.cs,Program.cs" /d:sonar.coverage.inclusions="**/*.cs" /d:sonar.cs.opencover.reportsPaths="Delivery.Tests/coverage.opencover.xml" ``` |
| meet de code coverage                                        | ```dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover  --logger "trx;LogFileName=TestResults.trx"```                                                                                                                                                                                                                                             |
| maak het rapport                                             | ```~/.dotnet/tools/dotnet-sonarscanner end```                                                                                                                                                                                                                                                                                                                       |